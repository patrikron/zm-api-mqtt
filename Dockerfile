FROM ruby:alpine

RUN apk add --no-cache mosquitto-clients jq curl

COPY zm.sh /opt/zm.sh
WORKDIR /opt
RUN chmod 777 /opt/zm.sh

CMD /opt/zm.sh