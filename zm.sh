#./bin/bash
mqtt_server=$MQTT_SERVER
mqtt_clientId=$MQTT_CLIENTID

touch cookies.txt

while true; do 

	curl -d "username=${ZM_USERNAME}&password=${ZM_PASSWORD}&action=login&view=console" -c cookies.txt  ${ZM_HOST}/zm/index.php > /dev/null

	DAEMON=$(curl -b cookies.txt ${ZM_HOST}/zm/api/host/daemonCheck.json)
	echo $DAEMON
	d=$(echo ${DAEMON} | jq '.result')

	echo $d
	if [[ $d != "null" ]]; then
	  mosquitto_pub -h $mqtt_server -m "$DAEMON" -t zm/daemon
	else 
		mosquitto_pub -h $mqtt_server -m '{ "result" : "0" }' -t zm/daemon
		echo "Error, ZM is down."
		exit 100
	fi

	# Load
	LOAD=$(curl -b cookies.txt ${ZM_HOST}/zm/api/host/getLoad.json)
	echo $LOAD
	l=$(echo ${LOAD} | jq '.load[0]')
	echo $l
	if [[ $l != "null" ]]; then	
	  	  mosquitto_pub -h $mqtt_server -m  """{\"load\":${l}}""" -t zm/load
	fi


 	# DISK
	DISKP=$(curl -b cookies.txt ${ZM_HOST}/zm/api/host/getDiskPercent.json)
	#echo $DISKP
	total=$(echo ${DISKP} | sed 's/^.*{ "usage"/{ "usage"/')
	total=$(echo ${total} | jq '.usage.Total.space')
	echo $total
	if [[ $total != "null" ]]; then
	mosquitto_pub -h $mqtt_server -m """{\"usage\":${total}}""" -t zm/disk
	fi

	sleep $UPDATE_INTERVALL

done